import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Todo } from '../interfaces';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {

  @Input()
  todo : Todo;

  @Output()
  delete : EventEmitter<Todo>;

  constructor() {
      this.delete = new EventEmitter();
  }

  ngOnInit() {
  }

  deleteItem(e: any) {
      this.delete.emit( this.todo );
  }

}
