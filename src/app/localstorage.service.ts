import { Injectable } from '@angular/core';

import { Project } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

  saveProjects( projects : Project[] ) : void {
      localStorage.setItem( 'projects', JSON.stringify(projects) );
  }

  loadProjects() : Promise<Project[]> {
      return new Promise( resolve => {
          let projects = JSON.parse( localStorage.getItem('projects') );
          if (projects) {
              resolve( projects );
          } else {
              resolve( [] );
          }
      } );
  }

}
